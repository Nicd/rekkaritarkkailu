defmodule Rekkaritarkkailu.StaticPageController do
  use Rekkaritarkkailu.Web, :controller

  @moduledoc """
  Controller for static pages that don't need any content from models.
  """

  plug :action
  plug :render

  def index(conn, _params) do
    conn
    |> assign(:title, "Etusivu")
  end

  def info(conn, _params) do
    conn
    |> assign(:title, "Tietoa harrastuksesta")
  end
end
