defmodule Rekkaritarkkailu.Router do
  use Phoenix.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Rekkaritarkkailu do
    pipe_through :browser # Use the default browser stack

    # Authentication
    get "/rekisterointi", AuthController, :register
    post "/rekisterointi", AuthController, :register_do

    get "/sisaan", AuthController, :login
    post "/sisaan", AuthController, :login_do

    get "/ulos", AuthController, :logout
    post "/salakala/uusi", AuthController, :request_password
    post "/salakala/resetoi", AuthController, :reset_password

    # Static pages
    get "/", StaticPageController, :index
    get "/tietoa", StaticPageController, :info
  end

  # Other scopes may use custom stacks.
  # scope "/api", Rekkaritarkkailu do
  #   pipe_through :api
  # end
end
