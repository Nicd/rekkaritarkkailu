defmodule Rekkaritarkkailu.Repo.Migrations.InitialUsersCreate do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string, size: 50
      add :email, :string, size: 255
      add :hash, :string, size: 130
      add :recovery_hash, :string, size: 130
      add :joined_at, :datetime
    end

    create index(:users, [:username], unique: true)
  end
end
