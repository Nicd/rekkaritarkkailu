defmodule Rekkaritarkkailu.EmailTemplates do
  def register_subject(_user) do
    "rekkaritarkkailu.fi: Tervetuloa rekkaritarkkailijaksi!"
  end

  def register_template(user) do
    """
      <h1>Tervetuloa rekkaritarkkailijaksi!</h1>

      Blaa: #{user.email}

      Terveisin,
      rekkaritarkkailu.fi ylläpito
    """
  end

  def password_recovery_subject(_user) do
    "rekkaritarkkailu.fi: Salasanan palautus"
  end

  def password_recovery_template(user) do
    """
      <h1>Salasanan palautuslinkki</h1>

      Olet pyytänyt salasanan palautusta sivustolla rekkaritarkkailu.fi. Voit palauttaa
      salasanasi <a href="http://yourawesomeapp.com/recover_password?token=#{user.recovery_hash}">klikkaamalla tätä linkkiä</a>.

      Mikäli et ole pyytänyt salasanan palautusta, voit jättää tämän viestin huomiotta.

      Terveisin,
      rekkaritarkkailu.fi ylläpito
    """
  end
end