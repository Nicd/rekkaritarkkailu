defmodule Rekkaritarkkailu.AuthLib do
  alias Rekkaritarkkailu.Repo
  alias Rekkaritarkkailu.User
  alias Comeonin.Bcrypt, as: Hasher
  import Plug.Conn, only: [get_session: 2, put_session: 3, clear_session: 1]
  import Ecto.Query, only: [from: 2]

  @moduledoc """
  Authentication related actions, such as registering users, logging in and out, recovering passwords etc.
  """

  @doc """
  Hash a given password.
  """
  def hash_password(password) do
    Hasher.hashpwsalt password
  end

  @doc """
  Check that the given password matches the stored hash.
  """
  def check_password(password, hash) do
    Hasher.checkpw password, hash
  end

  @doc """
  Create a new user

  Expects the user's details as a map with the following info:
    * username
    * email
    * password
  """
  def create_user(params) do
    password = Map.get params, "password"

    if password == nil or String.length(password) == 0 do
      {:error, :missing_password}
    else
      # Generate hash from given password
      params = Map.put params, "hash", hash_password(password)

      changeset = User.changeset %User{}, :create, params

      if changeset.valid? do
        new_user = Repo.insert changeset
        {:ok, new_user}
      else
        IO.inspect changeset
        {:error, changeset}
      end
    end
  end

  @doc """
  Returns the user struct of the current user or nil if user is not authenticated.
  """
  def current_user(conn) do
    get_session conn, :user
  end

  @doc """
  Returns true if user is currently authenticated, false if not.

  For cases where you don't need the user object, only their
  authentication status.
  """
  def is_authed(conn) do
    current_user(conn) != nil
  end

  @doc """
  Authenticate current connection as the given user.

  Returns the given conn.
  """
  def authenticate(conn, user) do
    # Store user information to session
    put_session conn, :user, user
  end

  @doc """
  Attempt to authenticate with the given username and password.

  After using this function, you need to call `authenticate` with
  the connection and the returned user.

  Will return {:ok, user} with the user details, or :Error if
  authentication failed for the wrong username or password.
  """
  def login(conn, username, password) do
    query = from u in User,
      where: u.username == ^username

    user = Repo.one query

    case user do
      nil ->
        Hasher.dummy_checkpw
        :error

      user ->
        if Hasher.checkpw password, user.hash do
          {:ok, user}
        else
          :error
        end
    end
  end

  @doc """
  Log out the current user.

  The session attached to the connection will be cleared, even if
  the user was not logged in.
  """
  def logout(conn) do
    clear_session conn
  end
end